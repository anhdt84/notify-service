--liquibase formatted sql
--changeset microservice_class:20230725_1

CREATE
EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE DEVICE_KEY
(
    ID         uuid PRIMARY KEY default uuid_generate_v4(),
    MOBILENUMBER    varchar(20),
    DEVICE_KEY    varchar(200),
    UPDATED_AT timestamp
);