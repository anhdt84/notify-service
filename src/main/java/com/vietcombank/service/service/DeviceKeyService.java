package com.vietcombank.service.service;

import com.vietcombank.service.entity.DeviceKeyEntity;
import com.vietcombank.service.repository.DeviceKeyEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class DeviceKeyService {
    @Autowired
    private DeviceKeyEntityRepository deviceKeyEntityRepository;

    public List<DeviceKeyEntity> retrieveAll() {
        return deviceKeyEntityRepository.findAll();
    }

    public void save(DeviceKeyEntity deviceKeyEntity) {
        deviceKeyEntityRepository.save(deviceKeyEntity);
    }

    public void delete(String id) {
        deviceKeyEntityRepository.deleteById(UUID.fromString(id));
    }

    public DeviceKeyEntity getById(String id) {
        return deviceKeyEntityRepository.findById(UUID.fromString(id)).get();
    }
}
